export const sidebarData = {
  name: 'Administrator',
  username: 'root',
  avatar_url: 'path/to/img_administrator',
  assigned_open_issues_count: 1,
  assigned_open_merge_requests_count: 2,
  todos_pending_count: 3,
  issues_dashboard_path: 'path/to/issues',
};
